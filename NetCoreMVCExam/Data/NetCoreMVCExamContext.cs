﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetCoreMVCExam.Models;

namespace NetCoreMVCExam.Models
{
    public class NetCoreMVCExamContext : DbContext
    {
        public NetCoreMVCExamContext (DbContextOptions<NetCoreMVCExamContext> options)
            : base(options)
        {
        }

        public DbSet<NetCoreMVCExam.Models.Category> Category { get; set; }

        public DbSet<NetCoreMVCExam.Models.Product> Product { get; set; }
    }
}
